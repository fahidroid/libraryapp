import { RegisterComponent } from './pages/common/register/register.component';
import { BookDetailComponent } from './pages/books/book-detail/book-detail.component';
import { UsersComponent } from './pages/users/users.component';
import { LibrarianComponent } from './pages/librarian/librarian.component';
import { BooksComponent } from './pages/books/books.component';
import { AuthGuard } from './guard/auth.guard';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/common/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,

  },
  {
    path: 'register',
    component: RegisterComponent,

  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'books',
    component: BooksComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'book/:id',
    component: BookDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'book/:id/:assignId',
    component: BookDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'librarians',
    component: LibrarianComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'readers',
    component: UsersComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
