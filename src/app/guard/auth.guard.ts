import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (localStorage.getItem('isLoggedIn')) {
      let user: any = localStorage.getItem('user');
      if (user.role == 'admin' || user.role == 'librarian') {
        this.router.navigate(['/books']);
      } else if (user.role == 'reader') {
        this.router.navigate(['/home']);
      }

      return true;
    }
    this.router.navigate(['/login']);
    // return false;
  }
}
