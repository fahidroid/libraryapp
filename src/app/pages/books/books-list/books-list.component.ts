import { UserService } from './../../../services/user.service';
import { UtilService } from './../../../services/util.service';
import { BookAssignedUsersListComponent } from './../../../modals/book-assigned-users-list/book-assigned-users-list.component';
import { AssignBookComponent } from './../../../modals/assign-book/assign-book.component';
import { AddBookComponent } from './../../../modals/add-book/add-book.component';
import { BooksService } from './../../../services/books.service';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Book } from '../book.model';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss'],
})
export class BooksListComponent {
  displayedColumns: string[] = [
    'name',
    'author',
    'category',
    // 'publishDate',
    'action',
  ];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  user: any
  constructor(
    private bookService: BooksService,
    private matDialog: MatDialog,
    private router: Router,
    private utilService: UtilService,
    private userService: UserService
  ) {
    // Create 100 users

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource([]);
    this.user = JSON.parse(localStorage.getItem('user'));

    this.getBooks();
  }

  assignBook(book) {
    let dialogRef = this.matDialog.open(AssignBookComponent, {
      width: '450px',
      disableClose: true,
      data: {
        book,
      },
    });
  }

  checkoutBook(book) {
    this.bookService
      .assignBook(this.user.id, book)
      .then((data) => {
        this.utilService.openSnackBar('Book Assigned Successfully');
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
      });
  }

  getBooks() {
    this.bookService.getBooks().subscribe((books) => {
      let bookList = books.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        } as Book;
      });

      this.dataSource = new MatTableDataSource(bookList);
    });
  }

  viewBookDetail(book) {
    this.router.navigate(['book', book.id]);
  }

  edit(row) {
    let dialogRef = this.matDialog.open(AddBookComponent, {
      width: '450px',
      disableClose: true,
      data: {
        edit: true,
        book: row,
      },
    });
  }

  assignedUsers(row) {

    console.log(row)
    this.bookService.getAssignBookByBookId(row.id).subscribe(data => {

      let assignedList = data.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        } as Book;
      });


      console.log(assignedList)

      if (assignedList.length <= 0) {

        this.utilService.openSnackBar("No user assigned");
        return
      }

      this.matDialog.open(BookAssignedUsersListComponent, {
        width: "700px",
        data: {
          tableData: assignedList,
        }
      })


    })


  }

  deleteBook(row) {
    this.bookService.deleteBook(row.id);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
