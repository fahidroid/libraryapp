import { RateCommentComponent } from './../../../modals/rate-comment/rate-comment.component';
import { BooksService } from 'src/app/services/books.service';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit {
  book: any;
  bookId: any;

  reviews: any = [];

  user: any;

  assignedBookId: any
  assignedBook: any
  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private bookService: BooksService,
    private dialog: MatDialog
  ) {
    this.route.paramMap.subscribe(async (result: any) => {
      this.bookId = result.params.id;
      this.assignedBookId = result.params.assignId;

      console.log(this.assignedBookId)

      await this.getBook();


    });

    this.user = JSON.parse(localStorage.getItem('user'));
  }

  private async getBook() {
    if (this.assignedBookId) {
      let assignedBook = await this.bookService.getAssignedBookById(this.assignedBookId).toPromise();

      this.assignedBook = assignedBook.data();
    }


    let b = await this.bookService.getBookById(this.bookId).toPromise();

    this.book = b.data();

    this.bookService.getRatingByBookId(this.bookId).subscribe(review => {

      let reviewssList = review.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        };
      });

      this.reviews = reviewssList;

    });
  }

  ngOnInit(): void { }

  rate() {
    this.dialog.open(RateCommentComponent, {
      width: '450px',
      data: {
        book: { id: this.bookId, ...this.book },
        user: this.user
      }
    })
  }

  return() {

    this.bookService.returnBook(this.assignedBookId)

    this.getBook()
  }

  navBack() {
    this.location.back();
  }
}
