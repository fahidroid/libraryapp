import { AddBookComponent } from './../../modals/add-book/add-book.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit {
  user: any
  constructor(private matDialog: MatDialog) {
    this.user = JSON.parse(localStorage.getItem('user'));

  }

  ngOnInit(): void { }

  addBook() {
    let dialogRef = this.matDialog.open(AddBookComponent, {
      width: '450px',
      disableClose: true,
      data: {
        edit: false,
      },
    });
  }
}
