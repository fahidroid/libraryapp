export class Book {
  id: string;
  name: string;
  category: string;
  author: string;
  publishDate: Date;
}
