import { AddUserComponent } from './../../../modals/add-user/add-user.component';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private matDialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    let payload = this.loginForm.getRawValue();

    this.authService.login(payload);
  }

  register() {

    this.router.navigate(['register']);
    // let dialogRef = this.matDialog.open(AddUserComponent, {
    //   width: '450px',
    //   disableClose: true,
    //   data: {
    //     edit: false,
    //     createReader:true,
    //     role: 'reader',
    //   },
    // });

    // dialogRef.afterClosed().subscribe((data) => {
    //   console.log(data);
    // });
  }
}
