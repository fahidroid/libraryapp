import { Component, OnInit } from '@angular/core';
import { DocumentReference } from '@angular/fire/firestore';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  createUserForm: FormGroup;;

  loading:boolean = false

  constructor(

    private fb: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createUserForm = this.fb.group({
      fName: new FormControl('', [Validators.required]),
      lName: new FormControl('', [Validators.required]),
      email: new FormControl('', [
        Validators.required,
      ]),
      phone: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
      ]),
    });
  }

  create() {
    let payload = this.createUserForm.getRawValue();

    payload['role'] = 'reader';

    this.loading = true;
    this.authService
      .register(payload)
      .then(async (user:DocumentReference) => {
        this.utilService.openSnackBar('User Created');

        this.router.navigate(['/login']);

        // this.authService.isLoggedIn.next(true);

        // let userData:any = (await user.get()).data
        // userData = {
        //   id: (await user.get()).id,
        //   ...userData,
        // };

        // console.log(userData)

        // localStorage.setItem('user', JSON.stringify(userData));
        // localStorage.setItem('isLoggedIn', 'true');
        // this.router.navigate(['/home']);
        // this.authService.currentUser.next(userData);
        // this.loading = false
        // window.location.replace('/home');

      })
      .catch((err) => {
        this.loading = false
        
        console.log(err)
        this.utilService.openSnackBar(err.message);
      });
  }
}
