import { AddUserComponent } from './../../modals/add-user/add-user.component';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-librarian',
  templateUrl: './librarian.component.html',
  styleUrls: ['./librarian.component.scss'],
})
export class LibrarianComponent implements OnInit {
  librarians: any;

  constructor(private matDialog: MatDialog, private userService: UserService) {}

  ngOnInit(): void {
    this.getLibrarians();
  }

  getLibrarians() {
    this.userService.getLibrarians().subscribe((librarian) => {
      let librariansList = librarian.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        };
      });

      console.log(librariansList)

      this.librarians = librariansList;
    });
  }

  addLibrarian() {
    let dialogRef = this.matDialog.open(AddUserComponent, {
      width: '450px',
      disableClose: true,
      data: {
        edit: false,
        role: 'librarian',
      },
    });
  }
}
