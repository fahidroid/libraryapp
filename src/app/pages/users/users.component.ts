import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddUserComponent } from 'src/app/modals/add-user/add-user.component';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  readers: any;

  constructor(private matDialog: MatDialog, private userService: UserService) {}

  ngOnInit(): void {
    this.getReaders();
  }

  getReaders() {
    this.userService.getReaders().subscribe((librarian) => {
      let readersList = librarian.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        };
      });

      this.readers = readersList;
    });
  }

  addReader() {
    let dialogRef = this.matDialog.open(AddUserComponent, {
      width: '450px',
      disableClose: true,
      data: {
        edit: false,
        role: 'reader',
      },
    });
  }
}
