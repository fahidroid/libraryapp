import { BooksService } from 'src/app/services/books.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  bookList;
  user:any;
  constructor(private bookService: BooksService, private router:Router) {
    this.user = JSON.parse(localStorage.getItem('user'));

    console.log(this.user)

  }

  async ngOnInit() {
    let user: any = JSON.parse(localStorage.getItem('user'));
    (await this.bookService.getAssignBook(user.id)).subscribe((books) => {

      let librariansList = books.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        };
      });

      this.bookList = librariansList;
      console.log(librariansList);

    });
  }

  navBookDetail(item){

    console.log(item)

    this.router.navigate(['book', item.book.id, item.id])
  }
}
