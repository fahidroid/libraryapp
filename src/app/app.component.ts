import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'libraQatar';

  constructor(private router: Router) {
    if (localStorage.getItem('isLoggedIn')) {
      let user: any = localStorage.getItem('user');
      if (user.role == 'admin' || user.role == 'librarian') {
        this.router.navigate(['/books']);
      } else if (user.role == 'reader') {
        this.router.navigate(['/home']);
      }

      return
    }
    this.router.navigate(['/login']);

  }
}
