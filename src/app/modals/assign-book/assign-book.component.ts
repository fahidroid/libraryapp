import { UserService } from './../../services/user.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BooksService } from 'src/app/services/books.service';
import { UtilService } from 'src/app/services/util.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-assign-book',
  templateUrl: './assign-book.component.html',
  styleUrls: ['./assign-book.component.scss'],
})
export class AssignBookComponent implements OnInit {
  userControl = new FormControl();
  options: any = [];
  filteredOptions: Observable<string[]>;

  book: any;

  constructor(
    public dialogRef: MatDialogRef<AssignBookComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private bookService: BooksService,
    private userService: UserService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.book = this.data.book;
    this.userService.getReaders().subscribe((readers) => {
      let readerList = readers.map((e) => {
        let data = e.payload.doc.data() as Object;
        return {
          id: e.payload.doc.id,
          ...data,
        };
      });

      this.options = readerList;

      this.filteredOptions = this.userControl.valueChanges.pipe(
        startWith(''),
        map((value) => this._filter(value))
      );
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }

  private _filter(value: string): string[] {
    console.log(value);
    const filterValue = value.toLowerCase();

    return this.options.filter((option) =>
      option.email.toLowerCase().includes(filterValue)
    );
  }

  submit() {
    console.log(this.userControl.value);

    let email = this.userControl.value;

    let user = this.options.filter((option) => option.email == email)[0];

    this.bookService
      .assignBook(user.id, this.book)
      .then((data) => {
        this.utilService.openSnackBar('Book Assigned Successfully');
        this.dialogRef.close();
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
      });
  }
}
