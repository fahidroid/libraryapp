import { UserService } from './../../services/user.service';
import { UtilService } from './../../services/util.service';
import { AuthService } from './../../services/auth.service';
import { Component, Inject, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  createUserForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private authService: AuthService,
    private utilService: UtilService,
    private userService: UserService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    console.log(this.data.role);
    this.createUserForm = this.fb.group({
      fName: new FormControl('', [Validators.required]),
      lName: new FormControl('', [Validators.required]),
      email: new FormControl({ value: '', disabled: this.data.edit }, [
        Validators.required,
      ]),
      phone: new FormControl('', [Validators.required]),
      password: new FormControl({ value: '', disabled: this.data.edit }, [
        Validators.required,
      ]),
    });

    if (this.data.edit) {
      this.createUserForm.patchValue({
        fName: this.data.user.fName,
        lName: this.data.user.lName,
        email: this.data.user.email,
        phone: this.data.user.phone,
      });
    }
  }

  submit() {
    if (this.data.edit) {
      this.edit();
    } else {
      this.create();
    }
  }

  create() {
    let payload = this.createUserForm.getRawValue();

    payload['role'] = this.data.role;

    this.authService
      .register(payload)
      .then((data) => {
        this.utilService.openSnackBar('User Created');

        if(this.data.createReader){
        this.dialogRef.close(
          data
        );

        }
        this.dialogRef.close();
      })
      .catch((err) => {

        console.log(err)
        this.utilService.openSnackBar(err.message);
      });
  }

  edit() {
    let payload = this.createUserForm.getRawValue();
    this.userService
      .updateUser(this.data.book.id, payload)
      .then((data) => {
        this.utilService.openSnackBar('User Updated Successfully');
        this.dialogRef.close();
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
        this.dialogRef.close();
      });
  }
}
