import { UtilService } from 'src/app/services/util.service';
import { BooksService } from 'src/app/services/books.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StarRatingComponent } from 'ng-starrating';

@Component({
  selector: 'app-rate-comment',
  templateUrl: './rate-comment.component.html',
  styleUrls: ['./rate-comment.component.scss'],
})
export class RateCommentComponent implements OnInit {
  comment: string;

  totalstar = 5
  starRating;
  constructor(
    public dialogRef: MatDialogRef<RateCommentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private bookService: BooksService,
    private utilService: UtilService,

  ) {

    console.log(this.data)
  }

  ngOnInit(): void { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  submit() {

    let payload = {
      book: this.data.book,
      user: this.data.user,
      comment: this.comment,
      start:this.starRating
    }

    console.log(payload)


    this.bookService.rateBook(payload).then((data) => {
      this.utilService.openSnackBar('Review Submitted');
      this.dialogRef.close();
    })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
        this.dialogRef.close();
      });


  }


  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    this.starRating = $event.newValue;
  }
}
