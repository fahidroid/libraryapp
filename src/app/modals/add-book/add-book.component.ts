import { UtilService } from './../../services/util.service';
import { BooksService } from './../../services/books.service';
import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent implements OnInit {
  createBookForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddBookComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private bookService: BooksService,
    private utilService: UtilService
  ) {
    console.log(this.data.book);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.createBookForm = this.fb.group({
      name: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
      author: new FormControl('', [Validators.required]),
      publishDate: new FormControl('', [Validators.required]),
    });

    if (this.data.edit) {
      let book = this.data.book;

      this.createBookForm.patchValue({
        name: book.name,
        category: book.category,
        author: book.author,
        publishDate: book.publishDate,
      });
    }
  }

  submit() {
    if (this.data.edit) {
      this.edit();
    } else {
      this.create();
    }
  }

  create() {
    let payload = this.createBookForm.getRawValue();

    this.bookService
      .createBook(payload)
      .then((data) => {
        this.utilService.openSnackBar('Book Created Successfully');
        this.dialogRef.close();
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
        this.dialogRef.close();
      });
  }

  edit() {
    let payload = this.createBookForm.getRawValue();
    this.bookService
      .updateBook(this.data.book.id, payload)
      .then((data) => {
        this.utilService.openSnackBar('Book Updated Successfully');
        this.dialogRef.close();
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
        this.dialogRef.close();
      });
  }
}
