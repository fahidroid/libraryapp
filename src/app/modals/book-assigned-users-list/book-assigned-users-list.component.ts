import { UtilService } from './../../services/util.service';
import { BooksService } from './../../services/books.service';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-book-assigned-users-list',
  templateUrl: './book-assigned-users-list.component.html',
  styleUrls: ['./book-assigned-users-list.component.scss']
})
export class BookAssignedUsersListComponent implements OnInit {
  tableData: any;

  displayedColumns: string[] = [
    'book',
    'user',
    'returned',
    'action',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    public dialogRef: MatDialogRef<BookAssignedUsersListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private bookService: BooksService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data.tableData);
  }


  unassign(row) {

    this.bookService.deleteAssignedBook(row.id).then((data) => {
      this.utilService.openSnackBar('Book Unassigned');
      this.dialogRef.close();
    })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
        this.dialogRef.close();
      });

  }
}
