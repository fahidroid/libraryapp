import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookAssignedUsersListComponent } from './book-assigned-users-list.component';

describe('BookAssignedUsersListComponent', () => {
  let component: BookAssignedUsersListComponent;
  let fixture: ComponentFixture<BookAssignedUsersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookAssignedUsersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAssignedUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
