import { UtilService } from './util.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { BehaviorSubject, Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser: BehaviorSubject<any> = new BehaviorSubject<any>({});

  isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(
    public afAuth: AngularFireAuth,
    public firestore: AngularFirestore,
    private router: Router,
    private utilService: UtilService
  ) { }

  async login(payload) {
    return new Promise<any>(async (resolve, reject) => {
      try {
        let loginRes = await this.afAuth.signInWithEmailAndPassword(
          payload.email,
          payload.password
        );

        this.firestore
          .collection('users', (ref) => ref.where('email', '==', payload.email))
          .snapshotChanges()
          .subscribe((user: any) => {
            user = user[0];

            let data = user.payload.doc.data() as Object;
            user = {
              id: user.payload.doc.id,
              ...data,
            };
            this.currentUser.next(user);

            this.isLoggedIn.next(true);

            localStorage.setItem('user', JSON.stringify(user));
            localStorage.setItem('isLoggedIn', 'true');

            if (user.role == 'admin' || user.role == 'librarian') {
              this.router.navigate(['/books']);
            } else {
              this.router.navigate(['/home']);
            }

            resolve(user[0]);
          });
      } catch (err) {
        this.utilService.openSnackBar(err.message);
      }
    });
  }

  async register(payload) {
    return new Promise<any>(async (resolve, reject) => {
      try {
        let signUpRes = await this.afAuth.createUserWithEmailAndPassword(
          payload.email,
          payload.password
        );

        payload['active'] = true;
        let user = await this.firestore.collection('users').add(payload);

        console.log((await user.get()).id);



        resolve(user);
      } catch (err) {
        reject(err);
      }
    });
  }

  logout() { }
}
