import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class BooksService {
  constructor(public firestore: AngularFirestore) { }

  getBooks() {
    return this.firestore.collection('books').snapshotChanges();
  }

  getBookById(id) {
    return this.firestore.collection('books').doc(id).get();
  }

  assignBook(userId, book) {
    return this.firestore.collection('assignedBook').add({
      userId,
      book,
    });
  }

  getAssignBook(userId) {
    return this.firestore
      .collection('assignedBook', (ref) => ref.where('userId', '==', userId))
      .snapshotChanges();
  }

  deleteAssignedBook(id) {
    return this.firestore.doc(`assignedBook/${id}`).delete();

  }


  getAssignBookByBookId(bookId) {
    return this.firestore
      .collection('assignedBook', (ref) => ref.where('book.id', '==', bookId))
      .snapshotChanges();
  }

  getAssignedBookById(assignedBookId) {
    return this.firestore.collection('assignedBook').doc(assignedBookId).get();
  }

  createBook(payload) {
    return this.firestore.collection('books').add(payload);
  }

  updateBook(id, payload) {
    return this.firestore.doc(`books/${id}`).update(payload);
  }

  deleteBook(id) {
    return this.firestore.doc(`books/${id}`).delete();
  }

  rateBook(payload) {
    return this.firestore.collection('rating').add(payload);
  }

  getRatingByBookId(bookId) {

    console.log(bookId)
    return this.firestore
      .collection('rating', (ref) => ref.where('book.id', '==', bookId))
      .snapshotChanges();
  }

  returnBook(assignBookId) {
    this.firestore.doc(`assignedBook/${assignBookId}`).update({
      returned: true
    })
  }


}
