import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { ReadVarExpr } from '@angular/compiler';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(public firestore: AngularFirestore) {}

  getLibrarians() {
    let librarians = this.firestore
      .collection('users', (ref) => ref.where('role', '==', 'librarian'))
      .snapshotChanges();

    return librarians;
  }

  getReaders() {
    let reader = this.firestore
      .collection('users', (ref) => ref.where('role', '==', 'reader'))
      .snapshotChanges();

    return reader;
  }

  searchReaders(name) {
    let reader = this.firestore
      .collection('users', (ref) =>
        ref.where('role', '==', 'reader').where('fName', '<=', name)
      )
      .snapshotChanges();

    return reader;
  }

  createUser(payload) {
    return this.firestore.collection('users').add(payload);
  }

  updateUser(id, payload) {
    console.log(id);
    return this.firestore.doc(`users/${id}`).update(payload);
  }

  deleteUser(id) {
    return this.firestore.doc(`users/${id}`).delete();
  }
}
