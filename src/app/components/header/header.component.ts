import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false;
  role: string;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.isLoggedIn = localStorage.getItem('isLoggedIn') ? true : false;
    let user: any = JSON.parse(localStorage.getItem('user'));

    if (user) {
      this.role = user.role;
    }
  }

  logout() {
    localStorage.clear();

    // this.router.navigate(['login']);
    window.location.replace('login');
  }
}
