import { UtilService } from './../../services/util.service';
import { AddUserComponent } from './../../modals/add-user/add-user.component';
import { UserService } from './../../services/user.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  @Input('tableData') tableData: any;

  displayedColumns: string[] = [
    'fName',
    'lName',
    'email',
    'phone',
    'active',
    'action',
  ];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userService: UserService,
    private matDialog: MatDialog,
    private utilService: UtilService
  ) {}

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.tableData);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(row) {
    let dialogRef = this.matDialog.open(AddUserComponent, {
      width: '450px',
      disableClose: true,
      data: {
        edit: true,
        role: row.role,
        user: row,
      },
    });
  }

  changeStatus(row) {
    let payload = row;

    payload['active'] = !payload['active'];

    console.log(payload);
    this.userService
      .updateUser(row.id, payload)
      .then((data) => {
        this.utilService.openSnackBar('Status Changes Successfully');
      })
      .catch((err) => {
        this.utilService.openSnackBar(err.message);
      });
  }
}
