import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginComponent } from './pages/common/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from './material/material.module';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { HomeComponent } from './pages/home/home.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { BooksComponent } from './pages/books/books.component';
import { BooksListComponent } from './pages/books/books-list/books-list.component';
import { AddBookComponent } from './modals/add-book/add-book.component';
import { AssignBookComponent } from './modals/assign-book/assign-book.component';
import { RateCommentComponent } from './modals/rate-comment/rate-comment.component';
import { LibrarianComponent } from './pages/librarian/librarian.component';
import { RegisterComponent } from './pages/common/register/register.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserComponent } from './modals/add-user/add-user.component';
import { UsersComponent } from './pages/users/users.component';
import { BookDetailComponent } from './pages/books/book-detail/book-detail.component';
import { RatingModule } from 'ng-starrating';
import { BookAssignedUsersListComponent } from './modals/book-assigned-users-list/book-assigned-users-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    SidebarComponent,
    BooksComponent,
    BooksListComponent,
    AddBookComponent,
    AssignBookComponent,
    RateCommentComponent,
    LibrarianComponent,
    RegisterComponent,
    UserListComponent,
    AddUserComponent,
    UsersComponent,
    BookDetailComponent,
    BookAssignedUsersListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RatingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
