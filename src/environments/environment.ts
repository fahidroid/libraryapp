// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDRUJ1DYyrFK7OP2UDwts8L_nUs8tDvJdE",
    authDomain: "libraryapp-ba94c.firebaseapp.com",
    databaseURL: "https://libraryapp-ba94c.firebaseio.com",
    projectId: "libraryapp-ba94c",
    storageBucket: "libraryapp-ba94c.appspot.com",
    messagingSenderId: "566324799455",
    appId: "1:566324799455:web:3027e3e76562bf0dad866a",
    measurementId: "G-39ZVCS2RPS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
